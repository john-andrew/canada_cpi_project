from django.contrib import admin

from .models import ConsumerPriceIndex


class ConsumerPriceIndexAdmin(admin.ModelAdmin):
    fields = ('id', 'year', 'item', 'value', 'pct_change')
    list_display = ('id', 'year', 'item', 'value', 'pct_change')
    readonly_fields = ('id',)

admin.site.register(ConsumerPriceIndex, ConsumerPriceIndexAdmin)