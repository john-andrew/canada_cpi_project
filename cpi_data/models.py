from django.db import models


class ConsumerPriceIndex(models.Model):
    year = models.PositiveIntegerField()
    item = models.CharField(max_length=255)
    value = models.FloatField(null=True)
    pct_change = models.FloatField(null=True)

    class Meta:
        ordering = ('item', 'year', 'value')