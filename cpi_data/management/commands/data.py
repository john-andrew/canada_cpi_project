import csv, itertools, json, os

from django.core.management.base import BaseCommand

from cpi_data.models import ConsumerPriceIndex


THIS_DIR = os.path.dirname(__file__)
CSV_FILE_PATH = os.path.join(THIS_DIR, '../../../static/cpi-data.csv')
LAST_ROW = 12608

class Command(BaseCommand):

    def get_data(self):
        cpi_list = []
        item_list = []

        with open(CSV_FILE_PATH) as f:
            reader = csv.DictReader(f)

            row_count = 1
            prev_item = ''
            curr_item = ''

            for row in reader:
                record = {}
                record['year'] = int(row['Ref_Date'])
                record['item'] = row['COMM']
                record['value'] = float(row['Value'])

                curr_item = record['item']

                # new item list case
                if prev_item and curr_item != prev_item:
                    prev_item = curr_item

                    cpi_list.append(item_list)
                    
                    item_list = []
                    item_list.append(record)

                # same item list
                else:
                    prev_item = curr_item
                    item_list.append(record)

                row_count += 1
                if row_count > LAST_ROW:
                    break

        # calculate and add percent change to data
        for item_list in cpi_list:
            item_list[0]['pct_change'] = None
            for i in range(1, len(item_list)):
                prev_value = item_list[i-1]['value']
                curr_value = item_list[i]['value']
                pct_change = (curr_value - prev_value) / prev_value * 100
                pct_change = round(pct_change, 2)
                item_list[i]['pct_change'] = pct_change

        flattened_cpis = list(itertools.chain.from_iterable(cpi_list))

        return flattened_cpis

    def write_data(self, data):
        for x in data:        
            cpi = ConsumerPriceIndex(year=x['year'],
                                    item=x['item'],
                                    value=x['value'],
                                    pct_change=x['pct_change'])
            cpi.save()

    def handle(self, *args, **options):
        data = self.get_data()
        self.write_data(data)