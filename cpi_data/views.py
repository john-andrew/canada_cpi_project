import json

from django.core import serializers
from django.http import HttpResponse
from django.shortcuts import render

from .models import ConsumerPriceIndex as CPI

# Create your views here.

def index(request):    
    qs = CPI.objects.all()
    item_types = []
    item_store = {}
    for x in qs:
        if x.item in item_store:
            continue
        item_store[x.item] = True
        item_types.append(x.item)
    item_types.remove('All-items')

    context = {
        'item_types': item_types
    }

    return render(request, 'cpi_data/index.html', context)

def get(request):
    qs = CPI.objects.all()
    data = serializers.serialize("json", qs)

    return HttpResponse(data, content_type='application/json') 