# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cpi_data', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='consumerpriceindex',
            name='pct_change',
            field=models.FloatField(null=True),
        ),
        migrations.AlterField(
            model_name='consumerpriceindex',
            name='value',
            field=models.FloatField(null=True),
        ),
    ]
