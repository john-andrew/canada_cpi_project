# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ConsumerPriceIndex',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', serialize=False, auto_created=True)),
                ('year', models.PositiveIntegerField()),
                ('item', models.CharField(max_length=255)),
                ('value', models.FloatField()),
                ('pct_change', models.FloatField()),
            ],
            options={
                'ordering': ('year', 'item', 'value'),
            },
        ),
    ]
